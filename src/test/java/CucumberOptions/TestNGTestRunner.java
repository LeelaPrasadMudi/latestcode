package CucumberOptions;

import org.testng.annotations.DataProvider;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="C:\\Users\\leela\\eclipse-workspace\\Securends\\src\\test\\java\\features\\CEM.feature",
glue ="stepDefinitions",monochrome=true,
tags="@Regression",
plugin= {"json:target/cucumber.json","com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})
//"rerun:target/failed_scenarios.txt","html:target/cucumber.html", 
public class TestNGTestRunner extends AbstractTestNGCucumberTests {
	@Override
	@DataProvider(parallel=true)
	public Object[][] scenarios()
	{
		return super.scenarios();
	}

	
}
