Feature: ILM login
@sanity
Scenario: ILM login test
Given User is already on login page
When user enters user name "admin@securends.com" and password "Charter@123"
Then user clicks on login button
And user is on homepage

@Regression
Scenario: ILM login test
Given User is already on login page
When user enters user name "leelaprasad.mudi@securends.com" and password "Unilever@123"
Then user clicks on login button
And user is on homepage
