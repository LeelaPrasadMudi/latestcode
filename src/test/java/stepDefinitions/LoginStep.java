package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.CemLogin;
import pageObjects.PageObjectManager;
import utils.TestContextSetup;

public class LoginStep {

	public WebDriver driver;
	TestContextSetup testContextSetup;
	CemLogin cemlogin;
	PageObjectManager pageObjectManager;
public LoginStep(TestContextSetup testContextSetup)
	{
		this.testContextSetup=testContextSetup;
		this.cemlogin =testContextSetup.pageObjectManager.getCemLogin();
			
	}
@Given("^User is already on login page$")

	public void User_is_already_on_login_page() throws InterruptedException {
		cemlogin.GetTitle();
		Thread.sleep(2000);
}
@When("^user enters user name \"([^\"]*)\" and password \"([^\"]*)\"$")
public void user_enters_user_name_something_and_password_something(String strArg1, String strArg2) throws Throwable  {
//	CemLogin cemlogin=new CemLogin(testContextSetup.driver);
	cemlogin.LoginUsername(strArg1);
	cemlogin.LoginPassword(strArg2);
	Thread.sleep(2000);
	System.out.println(strArg1);
    System.out.println(strArg2); 
}


@Then("^user clicks on login button$")
public void user_clicks_on_login_button() throws Throwable  {
	cemlogin.Submit();
	System.out.println("User logged in sucessfully");
	 Thread.sleep(4000);
}

@And("^user is on homepage$")
public void user_is_on_homepage()  {
    cemlogin.GetTitle();
    
    System.out.println("Home page populated");
}
	
	
	
}
