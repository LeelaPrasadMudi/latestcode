package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.text.DateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import utils.TestBase;
import utils.TestContextSetup;

public class Hooks  {
	
	TestContextSetup testContextSetup;
	public static Scenario scenario;
	Logger log = Logger.getLogger(this.getClass());
	String screenshotdir = System.getProperty("user.dir") + "/Screenshots/";

	public Hooks(TestContextSetup testContextSetup) {

		this.testContextSetup = testContextSetup;
	}

	@After
	public void AfterScenario() throws IOException {

		testContextSetup.testBase.WebDriverManager().quit();

	}

	@AfterStep
	public void afterSteps(Scenario scenario) throws InterruptedException, IOException {
		WebDriver driver = testContextSetup.testBase.WebDriverManager();
		log.info("Scenario success : " + scenario.getName());
		// File sourcePath= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// byte[] fileContent = FileUtils.readFileToByteArray(sourcePath);
		// scenario.embed(fileContent, "image/png", "test.png");
		// scenario.attach(fileContent, "image/png", "test.png");
		Date now = new Date();
		DateFormat shortDf = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

		String currentDate = shortDf.format(now).toString().replace("/", "_");
		currentDate = currentDate.toString().replace(" ", "_");
		currentDate = currentDate.toString().replace(":", "_");

		File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String dest = screenshotdir + "test" + "_" + currentDate + ".png";
		File destination = new File(dest);
		FileUtils.copyFile(sourcePath, destination);
		byte[] fileContent = FileUtils.readFileToByteArray(destination);
		scenario.attach(fileContent, "image/png", scenario.getId() + "test" + "_" + currentDate + ".png");

	}

	@AfterStep
	public void AddScreenshot(Scenario scenario) throws IOException {
		WebDriver driver = testContextSetup.testBase.WebDriverManager();
		if (scenario.isFailed()) {
			// screenshot
			File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			byte[] fileContent = FileUtils.readFileToByteArray(sourcePath);
			scenario.attach(fileContent, "image/png", "image");

		}

	}

}
