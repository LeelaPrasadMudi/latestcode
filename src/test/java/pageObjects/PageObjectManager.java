package pageObjects;

import org.openqa.selenium.WebDriver;

public class PageObjectManager {
	
//	public LandingPage landingPage;
//	public OffersPage offersPage;
	public WebDriver driver;
//	public CheckoutPage checkoutPage;
	public CemLogin cemlogin;
	public PageObjectManager(WebDriver driver)
	{
		this.driver = driver;
	}

	public CemLogin getCemLogin()
	{
		cemlogin=new CemLogin(driver);
		return cemlogin;
	}
}
