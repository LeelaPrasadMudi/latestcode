package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CemLogin {

	public WebDriver driver;

	public CemLogin(WebDriver driver)
	{
		this.driver = driver;
	}
	
	By Username=By.id("loginemail");
	By Password=By.id("loginpassword");
	By submitbtn=By.id("Login_submitt");
	By Homepage =By.xpath("//img[@alt='SecurEnds Logo']");
	
	public void LoginUsername(String name) 
	{
	driver.findElement(Username).sendKeys(name);	
	}
	public void LoginPassword(String name)
	{
		driver.findElement(Password).sendKeys(name);
	}
	
	public void Submit()
	{
		driver.findElement(submitbtn).click();
	}
	
	public String GetTitle()
	{
		return driver.findElement(Homepage).getText();
	}
}
